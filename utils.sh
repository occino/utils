#!/bin/bash

assert_parameter() {
    if [[ -z "$1" ]]; then
        parameter_description="$2"
        echo "Missing parameter '${parameter_description}'"
        exit 1
    fi
}

assert_file() {
    if [[ ! -f "$1" ]]; then
        echo "Missing file: '"$1"'"
        exit 1
    fi
}

assert_dir() {
    if [[ ! -d "$1" ]]; then
        echo "Missing directory: '"$1"'"
        exit 1
    fi
}

assert_env_variables() {
    local env_variables=("$@")
    for env_variable in "${env_variables[@]}"; do
        if [[ -z "${!env_variable+x}" ]]; then
            echo "Missing env variable '${env_variable}'"
            exit 1
        fi
    done
}

source_file() {
    assert_parameter "$1"
    if [[ -f "$1" ]]; then
        source "$1"
    fi
}

array_as_string() {
    local str="$(printf ", %s" "$@")"
    echo "${str:2}"
}

print_file_info() {
    local file_path="$1"
    assert_file "$1"

    local full_file_path="$(realpath "${file_path}")"
    local full_path="$(dirname "${full_file_path}")"

    local basename=$(basename "${file_path}")
    local filename="${basename%.*}"
    local extension="${basename##*.}"

    echo "Print file info for: '${file_path}'"
    echo " - full_file_path: ${full_file_path}"
    echo " - full_path: ${full_path}"
    echo " - basename: ${basename}"
    echo " - filename: ${filename}"
    echo " - extension: ${extension}"
}

assert_valid_parameter() {
    local parameter="$1"
    assert_parameter "$1"
    shift
    local valid_values=("$@")
    for valid_value in "${valid_values[@]}"; do
        if [[ "${valid_value}" == "${parameter}" ]]; then
            return 0
        fi
    done
    echo "Invalid parameter: '${parameter}' ($(array_as_string "${valid_values[@]}"))"
    exit 1
}

lastest_file_in_dir() {
    local directory="$1"
    local filter="$2"

    assert_dir "${directory}"

    if [[ -z "${filter}" ]]; then
        echo "$(ls -t "${directory}" | head -n1)"
    else
        echo "$(ls -t "${directory}" | grep "${filter}" | head -n1)"
    fi
}

assert_string_in_array() {
    assert_parameter "$1" "string"
    assert_parameter "$2" "array"
    local str="$1"
    shift
    local arr=("$@")

    for item in "${arr[@]}"; do
        if [[ "${item}" == "${str}" ]]; then
            return 0
        fi
    done

    # string not found in array
    echo "Invalid value '${str}' [$(array_as_string "${arr[@]}")]"

    exit 1
}

delete_hidden_files() {
    local directory="$1"
    assert_dir "${directory}"
    find "${directory}" -name ".*" -delete
    return "$?"
}

delete_empty_folder() {
    local directory="$1"
    assert_dir "${directory}"
    find "${directory}" -type d -empty -delete
    return "$?"
}

add_postfix_to_filename() {
    local file_path="$1"
    local postfix="$2"
    assert_file "$1"
    assert_parameter "$2"

    local full_file_path="$(realpath "${file_path}")"
    local full_path="$(dirname "${full_file_path}")"

    local basename=$(basename "${file_path}")
    local filename="${basename%.*}"
    local extension="${basename##*.}"

    local full_file_path_new="${full_path}/${filename}_${md5_hash}.${extension}"

    mv "${full_file_path}" "${full_file_path_new}"
    echo "${full_file_path_new}"
}

get_md5sum_from_file() {
    local file_path="$1"
    assert_file "$1"
    local md5_hash=($(md5sum "${archive_path}"))
    echo "${md5_hash}"
}

archive_mongodb_dump() {
    local mongodb_database=$1
    local mongodb_port=$2
    local target_directory=$3

    assert_parameter "${mongodb_database}" "mongodb_database"
    [[ -z "${mongodb_port}" ]] && mongodb_port=27017
    mkdir -p "${target_directory}"

    local archive_path="${target_directory}/mongodb_$(date +%Y-%m-%d_%H-%M-%S).tgz"

    local mongodb_dump_dir="dump"

    # clean up temp folder
    rm -rf "${mongodb_dump_dir}"

    if mongodump --port "${mongodb_port}" -d "${mongodb_database}" --out "${mongodb_dump_dir}"; then
        if tar -zcvf "${archive_path}" "${mongodb_dump_dir}"; then
            # set permissions
            if chmod a+rx "${archive_path}"; then
                echo "New mongodb-archive created: '${archive_path}'"
                result=0
            else
                echo "Error: Could not set permissions (a+rx) for '${archive_path}'"
            fi
        else
            echo "Error: Compressing '${mongodb_dump_dir}' failed with code: '${$?}'"
        fi
    else
        echo "Error: mongodump failed with code '${$?}'"
    fi

    # clean up temp folder
    rm -rf "${mongodb_dump_dir}"

    return 0
}

archive_directory_content() {
    local result=1
    local source_directory=$1
    local target_directory=$2
    local prefix=$3

    assert_dir "${source_directory}"
    mkdir -p "${target_directory}"
    [[ -z "${prefix}" ]] && prefix="$(basename "${source_directory}")"

    local archive_path="${target_directory}/${prefix}_$(date +%Y-%m-%d_%H-%M-%S).tar"

    local mongodb_dump_dir="dump"

    # clean static dir before archiving
    delete_hidden_files "${source_directory}"

    # go inside the directory to get right archive structure
    cd "${source_directory}"

    if tar -cvf "${archive_path}" "."; then
        # set permissions
        if chmod a+rx "${archive_path}"; then

            local md5_hash=$(get_md5sum_from_file "${archive_path}")
            local files_found_with_md5="$(find "${target_directory}" -name "*${md5_hash}*" | wc -l)"

            if [[ "${files_found_with_md5}" -eq 0 ]]; then
                local new_archive_path=$(add_postfix_to_filename "${archive_path}" "${md5_hash}")
                echo "New static-archive created: '${new_archive_path}'"
            else
                rm "${archive_path}"
                echo "Found archive file with the same checksum (${md5_hash}) (duplicate). Remove new created file."
            fi

            result=0
        else
            echo "Error: Could not set permissions (a+rx) for '${archive_path}'"
        fi
    else
        echo "Error: Archiving '${source_directory}' failed with code '${$?}'"
    fi

    cd -

    return "${result}"
}

restore_mongodb_archive() {
    local result=1
    local archive_path=$1
    local mongodb_port=$2

    assert_file "${archive_path}"
    [[ -z "${mongodb_port}" ]] && mongodb_port=27017

    local mongodb_dump_dir="dump"

    # clean up old temp folder
    rm -rf "${mongodb_dump_dir}"

    if tar -xvzf "${archive_path}"; then
        if mongorestore --port "${mongodb_port}" --drop --verbose "${mongodb_dump_dir}"; then
            echo "Restoring database from archive succeeded"
            result=0
        else
            echo "Error: mongorestore failed with code: '${$?}'"
        fi
    else
        echo "Error: Could not extract '${archive_path}'"
    fi

    # clean up temp folder
    rm -rf "dump"

    return "${result}"
}

restore_from_archive() {
    local result=1
    local archive_path="$1"
    local static_path="$2"
    assert_file "${archive_path}"

    mkdir -p "${static_path}"

    # remove old static dir content
    cd "${static_path}"
    rm -rf *

    if tar -xvf "${archive_path}"; then
        echo "Restoring static folder succeeded"
        result=0
    else
        echo "Error: Could not extract '${archive_path}'"
    fi

    cd -

    return "${result}"
}

git_clone_repository() {
    local
    echo "git_clone_repository"
}

git_repositories() {
    assert_parameter "$1" "action [clone, update]"
    assert_parameter "$2" "git base address"
    assert_parameter "$3" "git namespace"
    assert_parameter "$4" "repositories"

    local action="$1"
    local base_address="$2"
    local namespace="$3"
    shift
    shift
    shift
    local repositories=("$@")
    for repository in "${repositories[@]}"; do
        local repository_name="${namespace}-${repository}"
        case "${action}" in
        "clone")
            if [[ ! -d "${repository_name}" ]]; then
                local git_address="${GIT_BASE_ADDRESS}/${repository_name}.git"
                echo "Cloning '${repository_name}'... from '${git_address}'"
                # git clone --recurse-submodules --quiet "${git_address}"

                if [[ "$?" -ne 0 ]]; then
                    echo "Cloning repository '${git_address}' failed"
                    return 1
                fi
            else
                echo "Warning: Repository '${repository_name}' already exists."
            fi
            ;;
        "update")
            if [[ -d "${repository_name}" ]]; then
                cd "${repository_name}"
                git pull
                git submodule update --recursive
                cd ..
            else
                echo "Error: Repository '${repository_name}' does not exist. Update failed."
                return 1
            fi
            ;;
        *)
            echo "Error: Unknown git action '${action}'"
            return 1
            ;;
        esac
    done
    return 0
}

execute_command_in_docker_container() {
    local container_name="$1"
    local command="$2"
    assert_parameter "${container_name}" "container name"
    assert_parameter "${command}" "command"
    echo "EXECUTING in DOCKER CONTAINER (${container_name}): '${command}'"
    if docker exec -i "${container_name}" bash -c "${command}"; then
        echo "COMMAND in DOCKER CONTAINER succeeded"
    else
        echo "COMMAND in DOCKER CONTAINER failed"
    fi
    return "$?"
}
